pragma solidity ^0.4.9;
import 'zeppelin-solidity/contracts/ownership/Ownable.sol';


contract SampleCoin is Ownable {

	string public name;
	string public symbol;
	uint256 public totalSupply;

	uint256 public sellPrice;
	uint256 public buyPrice;
	uint8 public decimals;

	address public minter;
	mapping (address => uint) public balances;

	event Sent(address from, address to, uint amount);

	event Burn(address indexed from, uint256 value);

	function SampleCoin() {
		minter = msg.sender;
		totalSupply = 100;
		balances[minter] = totalSupply;
		sellPrice = 1;
		buyPrice = 1000;
		name = "SampleCoin";
		symbol = "CC";
		decimals = 2;
	}

	function mint(address receiver, uint amount) onlyOwner {
		balances[receiver] += amount;
		totalSupply += amount;
	}

	function send(address receiver, uint amount) {
		if(balances[msg.sender] < amount) return;
		balances[msg.sender] -= amount;
		balances[receiver] += amount;
		Sent(msg.sender, receiver, amount);
	}

	function balances(address _account) returns (uint) {
		return balances[_account];
	}

	function burn(address from, uint value) onlyOwner returns (bool success) {
		require(balances[from] > value);

		balances[from] -= value;
		totalSupply -= value;
		Burn(from, value);
		return true;
	}

	function totalSupply() returns (uint256) {
		return totalSupply;
	}

	function setPrices(uint256 newSellPrice, uint256 newBuyPrice) onlyOwner {
		sellPrice = newSellPrice;
		buyPrice = newBuyPrice;
	}

	function getContractAddress() returns (address) {
		return this;
	}

	function buy() payable returns (uint amount){
		amount = msg.value / buyPrice;
		if (balances[minter] < amount) {
			revert();
		}
		balances[msg.sender] += amount;
		balances[minter] -= amount;
		Sent(minter, msg.sender, amount);
		return amount;
	}

	function sell(uint amount) returns (uint revenue){
		require(balances[msg.sender] < amount);
		balances[minter] += amount;
		balances[msg.sender] -= amount;
		revenue = amount * sellPrice;
		if (!msg.sender.send(revenue)) {
			revert();
		} else {
			Sent(msg.sender, minter, amount);
			return revenue;
		}
	}

	function() payable {
		buy();
	}

}
