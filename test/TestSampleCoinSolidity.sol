pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/SampleCoin.sol";

contract TestSampleCoinSolidity {

	SampleCoin testCoin = SampleCoin(DeployedAddresses.SampleCoin());

	function testInitialBalanceIsSet() {

		uint expected = 100;

		Assert.equal(testCoin.balances(tx.origin), expected, "initial balance is wrong");
	}

}