var SampleCoin = artifacts.require("./SampleCoin.sol");

contract('SampleCoinTestJs', function(accounts) {

	it('should put initial balance in first account', function() {
		return SampleCoin.deployed().then(function(instance) {
			return instance.balances.call(accounts[0]);
		}).then(function(balance) {
			assert.equal(balance.valueOf(), 100, "wrong initial balance in first account");
		});
	});

	it('should return contract address', function() {
		var st;
		return SampleCoin.deployed().then(function(instance) {
			st = instance;
			return st.getContractAddress.call();
		}).then(function(address) {
			return st.balances.call(address);
		}).then(function(balance) {
		});
	});

	it('should have total supply', function() {
		return SampleCoin.deployed().then(function(instance) {
			return instance.totalSupply.call();
		}).then(function(balance) {
			assert.equal(balance.valueOf(), 100, "wrong total supply");
		});
	});

	it('should buy tokens with buy function', function() {
		var accountOne = accounts[0];
		var accountTwo = accounts[1];
		var st;

		return SampleCoin.deployed().then(function(instance) {
			st = instance;
			return st.balances.call(accountTwo);
		}).then(balance => {
			assert.equal(balance.valueOf(), 0, "second account should have initial zero balance");
			return st.buy({from: accountTwo, value: web3.toWei(5000, "wei")});
		}).then(() => {
			return st.balances.call(accountTwo);
		}).then(function(balance) {
			assert.equal(5, balance.valueOf(), "tokens should be bought");
			return st.balances.call(accountOne);
		}).then(function(balance) {
			assert.equal(95, balance.valueOf(), "token value of owner should decrease");
			assert.equal(5000, web3.eth.getBalance(st.address), "contract should have ether that has been sent to it");
		});
	});

	it('should burn tokens', function() {
		var st;
		var accountOne = accounts[0];
		var accountTwo = accounts[1];

		return SampleCoin.deployed().then(function(instance) {
			st = instance;
			return st.balances.call(accountTwo);
		}).then(function(balance) {
			assert.equal(5, balance.valueOf(), "account two should have 5 tokens");
			return st.burn(accountTwo, 3);
		}).then(function(isSuccess) {
			return st.totalSupply.call();
		}).then(function(balance) {
			assert.equal(97, balance.valueOf(), "tokens should be burned");
			return st.balances.call(accountTwo);
		}).then(function(balance) {
			assert.equal(2, balance.valueOf(), "tokens should be burned");
		});
	});

	it('should buy tokens on send to token address', function() {
		var accountOne = accounts[0];
		var accountTwo = accounts[2];
		var st;

		return SampleCoin.deployed().then(function(instance) {
			st = instance;
			return st.balances.call(accountTwo);
		}).then(function(balance) {
			assert.equal(balance.valueOf(), 0, "second account should have initial zero balance");
			return st.sendTransaction({from: accountTwo, gas: 300000, value: web3.toWei(5000, "wei")});
		}).then(function()  {
			return st.balances.call(accountTwo);
		}).then(function(balance) {
			assert.equal(5, balance.valueOf(), "second account should have initial zero balance");
			assert.equal(10000, web3.eth.getBalance(st.address), "contract should have ether that has been sent to it");
		});
	});

});